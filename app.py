from flask import Flask
import requests

app = Flask(__name__)

@app.route('/')
def index():
    resposta_bd = requests.get("http://database/carros-excelentes")
    return str(resposta_bd.json())
